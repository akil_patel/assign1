// import dependencies 
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator");

const shirtPrice = 12.0;    
const pentPrice = 8.0;      
const hundred = 100;
const zero = 0;
const ten = 10;

const taxTiers = {
    alberta: {
        name: "Alberta",
        tax: 0.05,
    },
    bc: {
        name: "British Columbia",
        tax: 0.12,
    },
    manitoba: {
        name: "Manitoba",
        tax: 0.12,
    },
    nb: {
        name: "New Brunswick",
        tax: 0.15,
    },
    nf: {
        name: "Newfoundland",
        tax: 0.15,
    },
    nw: {
        name: "Northwest Territories",
        tax: 0.05,
    },
    ns: {
        name: "Nova Scotia",
        tax: 0.15,
    },
    nunavat: {
        name: "Nunavat",
        tax: 0.05,
    },
    on: {
        name: "Ontario",
        tax: 0.13,
    },
    pai: {
        name: "Prince Edward Island",
        tax: 0.15,
    },
    quebec: {
        name: "Quebec",
        tax: 0.14,
    },
    saskatchewan: {
        name: "Saskatchewan",
        tax: 0.11,
    },
    yukon: {
        name: "Yukon",
        tax: 0.05,
    },
};

var expressApp = express();
expressApp.use(express.urlencoded({ extended: false }));

expressApp.use(express.json());

expressApp.set("views", path.join(__dirname, "views"));
expressApp.use(express.static(__dirname + "/public"));
expressApp.set("view engine", "ejs");

expressApp.get("/", function (req, res) {
    res.render("form"); 
});

expressApp.post(
    "/",
    [
        check("name", "Name is required!").notEmpty(),
        check(
            "pent",
            "Please enter the amount of pent you want to buy"
        ).notEmpty(),
        check(
            "pent",
            "Please enter a valid quantity of pent"
        ).isNumeric(),
        check("shirt", "Please enter some value").notEmpty(),
        check(
            "shirts",
            "Please enter a valid quantity of shirt"
        ).isNumeric(),
        check("email", "Email is required").isEmail(),
        check("phone", "").custom(validatePhoneNumber), 
        check("province", "").custom(customProvinceValidation),
    ],
    function (req, res) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.render("form", {
                errors: errors.array(),
            });
        } else {
            var name = req.body.name;
            var email = req.body.email;
            var address = req.body.address;
            var city = req.body.city;
            var phone = req.body.phone;
            var shirtQuantity = req.body.shirt;
            var pentQuantity = req.body.pent;
            var shirtTotal = shirtQuantity * shirtPrice;
            var pentTotal = pentQuantity * pentPrice;
            var province = req.body.province;
            var provincialTax = taxTiers[province].tax;
            var subTotal = shirtTotal + pentTotal;

            if (subTotal <= 10) {
                var insufficientBuyError = {
                    value: "",
                    msg: "Please buy a minimum of 10$ products",
                    param: "",
                    location: "body",
                };
                var errorList = errors.array();

                errorList.push(insufficientBuyError);
                res.render("form", {
                    errors: errorList,
                });
            } else {
                var tax = subTotal * provincialTax;
                var total = subTotal + tax;

                var pageData = {
                    name: name,
                    email: email,
                    phone: phone,
                    address: address,
                    city: city,
                    shirt: shirtQuantity * shirtPrice,
                    pent: pentQuantity * pentPrice,
                    subTotal: subTotal,
                    province: taxTiers[province].name,
                    provincialTax: provincialTax * hundred,
                    tax: tax,
                    total: total,
                };
                res.render("form", pageData);
            }
        }
    }
);

function customProvinceValidation(value) {
    if (value.length == zero) {
        throw new Error("Kindly select atleast one province");
    }
    return true;
}


function validatePhoneNumber(value) {
    if (value.length != ten) {
        throw new Error("Cell number should atleast have 1o digits long. Like this xxxxxxxxxx");
    }
    return true;
}


expressApp.listen(8080);

console.log("Everything executed fine.. website at port 8080....");
