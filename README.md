
How to build install and use my project:

Node.js must be installed in the system.
Clone this repository.
Open terminal and write npm install.
Then, write index.js
Open browser and write localhost:8080
You can see the project running.
About License:

I have added Apache License 2.0 because its open source, OSI-approved and popular.